## Email Address List Generator

Generate sample email addresses based on random people names.

Find it https://ealg-253303.appspot.com/


Future deployements via command:

`gcloud app deploy .\bin\Release\netcoreapp2.2\publish\app.yaml`
