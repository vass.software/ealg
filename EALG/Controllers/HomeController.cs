﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EALG.Models;

namespace EALG.Controllers
{
    public class HomeController : Controller
    {
        //TODO allow pasting list of names in csv format
        public IActionResult Index() => View();

        [HttpPost]
        public IActionResult Index(EmailSyntaxParser model) 
        {
            NameGenerator ng = NameGeneratorFactory.CreateNameGenerator();
            List<Person> people = ng.GetRandomPeople(6);
            model.EmailAddresses = model.GenerateEmailAddresses(model.EmailSyntaxString, 
                                                                model.EmailDomain,
                                                                people);

                                    
                        
            return View(model);
        }

        public IActionResult Privacy() => View();
        public IActionResult Usage() => View();

        public IActionResult Source() => View();
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
