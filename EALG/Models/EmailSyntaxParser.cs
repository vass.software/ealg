using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;
namespace EALG.Models 
{
    public class EmailSyntaxParser
    {   
        public String EmailSyntaxString {get; set;}
        public String EmailDomain {get; set;}
        public List<String> EmailAddresses = new List<string>();
        // TODO add support for random numbers from 0-9
        public EmailSyntaxParser() 
        {  }
        public List<String> GenerateEmailAddresses(String syntaxString, String domainString, List<Person> people)
        {
            List<String> emailAddresses = new List<String>();
            StringBuilder emailBuilder = new StringBuilder();
            foreach (Person person in people)
            {
                emailBuilder.Clear();
                emailBuilder.Append(syntaxString);

                emailBuilder.Replace("{firstname}", person.name);
                emailBuilder.Replace("{lastname}", person.surname);

                emailBuilder.Append("@");
                emailBuilder.Append(domainString);   
                emailBuilder.Append(";");
                emailBuilder.Append(Environment.NewLine);
                Console.WriteLine(emailBuilder.ToString());             
                emailAddresses.Add(emailBuilder.ToString());
            }

            return emailAddresses;
        }
    }
}