using System;
using System.Net;
using System.Net.Http;
using System.Collections.Generic;
using System.Text;

namespace EALG.Models
{
    class NameGenerator 
    {
        internal String BaseURL = @"https://uinames.com/api/";
        public List<Person> GetRandomPeople(byte amountOfPeople)
        {
            List<Person> People = new List<Person>();
            String peopleJson = this.GetPeopleJson(amountOfPeople);
            People = Person.DeserializePeople(peopleJson);
            return People;
        }

        private String BaseURLBuilder(String country, String gender, byte amount)
        {
            StringBuilder baseURLBuilder = new StringBuilder();
            baseURLBuilder.Append(this.BaseURL);
            baseURLBuilder.Append("?region=");
            baseURLBuilder.Append(country);
            baseURLBuilder.Append("&amount=");
            baseURLBuilder.Append(amount);
            baseURLBuilder.Append("&gender=");
            baseURLBuilder.Append(gender);

            return baseURLBuilder.ToString();
        }

        private String GetPeopleJson(byte amountOfPeople)
        {
            String jsonResult;
            String url = BaseURLBuilder("australia", "male", amountOfPeople);
            try
            {
                using (var client = new HttpClient(new HttpClientHandler
                { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
                {
                    client.BaseAddress = new Uri(url);
                    HttpResponseMessage response = client.GetAsync("").Result;
                    response.EnsureSuccessStatusCode();
                    string result = response.Content.ReadAsStringAsync().Result;
                    jsonResult = result;
                }
            }
            catch (HttpRequestException ex) 
            {
                // If HTTP rquest fails, will cause deserialization to fail
                // TODO handle System.RunTtime.Serialization.SerialException
                jsonResult = "Error " + ex.Message; 
            }

            return jsonResult;
        }

    }
        
}

    enum Genders
    {
        female,
        male
    }
