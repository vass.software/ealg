using System;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace EALG.Models
{
    [DataContract]
    public class Person
    {
        // Example json
        // {"name":"Milada","surname":"Marcinek","gender":"female","region":"Slovakia"}
        // Returned from https://uinames.com/api/";

        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string surname { get; set; }
        [DataMember]
        public string gender { get; set; }
        [DataMember]
        public string region { get; set; }

        public Person Deserialize(String personJson) 
        {
            var deserializedPerson = new Person();
            var memStream = new MemoryStream(Encoding.UTF8.GetBytes(personJson));
            
            var service = new DataContractJsonSerializer(deserializedPerson.GetType());
            deserializedPerson = service.ReadObject(memStream) as Person;

            memStream.Close(); // replace with using
            return deserializedPerson;
        }

        public static List<Person> DeserializePeople(String peopleJson)
        {
            var deserializedPeople = new List<Person>();
            var memStream = new MemoryStream(Encoding.UTF8.GetBytes(peopleJson));
            Console.WriteLine(peopleJson);
            var service = new DataContractJsonSerializer(deserializedPeople.GetType());
            deserializedPeople = service.ReadObject(memStream) as List<Person>;

            memStream.Close(); // replace with using
            return deserializedPeople;
        }

        public override String ToString()
        {
            String result = this.name + " " + this.surname + " " + this.gender + " " + this.region;
            return result;
        }
    }  
}